class TreeNode<T : Comparable<T>>(val value: T,
                                  var left:TreeNode<T>? = null,
                                  var right:TreeNode<T>? = null)

class BinTree<T : Comparable<T>>(var root:TreeNode<T>? = null){
    private fun forEachAscend(startNode:TreeNode<T>?, callback: (T) -> Unit){
        if (startNode != null){
            forEachAscend(startNode.left, callback)
            startNode.let {callback(it.value)}
            forEachAscend(startNode.right, callback)
        }
    }

    fun printTree(callback: (T) -> Unit){
        forEachAscend(root, callback)
    }

    operator fun plus(value: T){
        if (root == null) {
            root = TreeNode(value)
        } else{
            var curNode: TreeNode<T>? = root
            var nextNode: TreeNode<T>? = root

            while (nextNode != null) {
                curNode = nextNode
                nextNode =
                    if (value == nextNode.value) {
                        return
                    }else if (value < nextNode.value){
                        nextNode.left
                    } else {
                        nextNode.right
                    }
            }

            curNode?.let {
                if (value < curNode.value) {
                    curNode.left = TreeNode(value)
                } else {
                    curNode.right = TreeNode(value)
                }
            }
        }
    }
}