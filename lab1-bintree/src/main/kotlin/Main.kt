//import com.sun.source.tree.BinaryTree

fun main(args: Array<String>) {
    println("Please type the numbers to store in binary tree, one by line")

    val tree = BinTree<Int>()
    var num:Int

    do {
        num = readLine()!!.toInt()

        if (num != 0) tree + num
    } while (num != 0)

    tree.printTree { print("$it ")}
    println()
}