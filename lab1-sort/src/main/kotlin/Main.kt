fun <T: Comparable<T>>ArrayInsertSort(array: Array<T>){
    var currentIndex: Int
    var sortedIndex = 1

    var temp:T

    while (sortedIndex < array.size){
        currentIndex = sortedIndex

        while (currentIndex != 0){
            if (array[currentIndex] < array[currentIndex - 1]){
                temp = array[currentIndex - 1]
                array[currentIndex - 1] = array[currentIndex]
                array[currentIndex] = temp
            } else {
                break
            }

            currentIndex--
        }

        sortedIndex++
    }
}

fun <T: Comparable<T>> Array<T>.InsertSort(){
    ArrayInsertSort(this)
}

fun main() {
    val array:Array<Int> = arrayOf(1, 2 , 7, 3, 4, 4, 5, 6, 7, 8, 9)
    val array2:Array<Int> = arrayOf(890, 3, 5, 76, 4, 4 , 35, 34, 54, 45)

    array.forEach { print("$it ") }
    println()

    array2.forEach { print("$it ") }
    println()


    println()
    ArrayInsertSort(array)
    array2.InsertSort()


    array.forEach { print("$it ") }
    println()

    array2.forEach { print("$it ") }
    println()


    println("Please type the numbers to store in array, one by line")

    var array3:Array<Int> = arrayOf()
    var num:Int

    do {
        num = readLine()!!.toInt()

        if (num != 0) array3 += num
    } while (num != 0)

    array3.InsertSort()
    array3.forEach { print("$it ") }
    println()
}